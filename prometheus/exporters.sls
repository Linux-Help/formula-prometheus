{% from "prometheus/map.jinja" import exporters with context %}
{%- for exporter, parameters in exporters.iteritems() %}
  {%- if parameters.get('enabled', False) %}
{{ exporter }}-exporter-bin-dir:
  file.directory:
    - name: /opt/prometheus/{{ exporter }}/bin
    - makedirs: True

{{ exporter }}_exporter_extracted:
  archive.extracted:
    - name: /opt/prometheus/{{ exporter }}/bin
    - source: {{ parameters.url }}
    - source_hash: {{ parameters.url_hash }}
    - options: {{ parameters.get('extract_options', None) }}
    - enforce_toplevel: False
    - user: root
    - group: root
    - if_missing: /opt/prometheus/{{ exporter }}/bin/{{ parameters.check }}
    - require:
      - file: {{ exporter }}-exporter-bin-dir

{#
{{ exporter }}_exporter_service:
  service.running:
    - name: {{ exporter }}_exporter
    - enable: True
    {%- if grains.get('noservices') %}
    - onlyif: /bin/false
    {%- endif %}
    {%- if parameters.template is defined %}
    - watch:
      - file: {{ exporter }}_exporter_service_config_file
    {%- endif %}
#}

    {%- for svc, svc_parameters in parameters.get('services', {}).iteritems()  %}
      {%- if  svc_parameters.get('enabled', False) %}

        {%- for svc_directory in svc_parameters.get('directories', []) %}
{{ exporter }}_{{ svc }}_{{ svc_directory.replace('/', '_') }}_exporter_directories:
  file.directory:
    - name: {{ svc_directory }}
    - user: {{ svc_parameters.get('user', 'root') }}
    - group: {{ svc_parameters.get('group', 'root') }}
    - makedirs: True
    - require_in:
      - file: {{ exporter }}_{{ svc }}_exporter_systemd_unit
      - service: {{ exporter }}_{{ svc }}_exporter_service
        {%- endfor %}

{{ exporter }}_{{ svc }}_exporter_systemd_unit:
  file.managed:
    - name: /etc/systemd/system/{{ svc }}.service
    - template: jinja
    - source:
      - salt://{{ svc_parameters.get('template', 'prometheus/files/exporter/service') }}
    - context:
      exporter: {{ exporter }}
      svc_name: {{ svc }}
      svc_args: {{ svc_parameters.args }}
      user: {{ svc_parameters.get('user', 'root') }}
      group: {{ svc_parameters.get('group', 'root') }}
    - user: root
    - group: root
    - mode: 0644
    - require:
      - archive: {{ exporter }}_exporter_extracted
    - watch_in:
      - service: {{ exporter }}_{{ svc }}_exporter_service

{{ exporter }}_{{ svc }}_exporter_service:
  service.running:
    - name: {{ svc}}
    - enable: True

{#
{{ _exporter_serviceorter }}_{{ svc }}_exporter_config_file:
  file.managed:
    - name: /etc/exporters/{{ exporter }}_{{ svc }}-running.yml
    - template: jinja
    - source:
      - salt://{{ svc_parameters.template }}
    - context:
      jmxbind: {{ jmxbind }}
    - user: root
    - group: root
    - mode: 644
    - require:
    {%- if parameters.get('packages', False) %}
      - pkg: {{ exporter }}_exporter_packages
    {%- else %}
      - archive: {{ exporter }}_exporter_extracted
    {%- endif %}
    - watch_in:
      - service: {{ exporter }}_exporter_service
#}
{#
        {%- if parameters.template is defined %}
          {%- set template = parameters.template %}
          {%- set bind = svc_parameters.get('bind', {}) %}
{{ exporter }}_exporter_service_config_file:
  file.managed:
    - name: /etc/default/{{ exporter }}-exporter
    - template: jinja
    - source:
      - salt://{{ template }}
    - context:
      bind: {{ bind }}
      cfg_file: /etc/exporters/{{ exporter }}_{{ svc }}-running.yml
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: {{ exporter }}_exporter_packages
    - watch_in:
      - service: {{ exporter }}_exporter_service
        {%- endif %}
#}
      {%- endif %}
    {%- endfor %}
  {%- endif %}
{%- endfor %}
